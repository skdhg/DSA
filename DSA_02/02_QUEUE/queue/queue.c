#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define CAPACITY 5

int FRONT = -1, REAR = -1;
int QUEUE[CAPACITY];

bool is_empty() {
    if (FRONT == -1) {
        return true;
    } else {
        return false;
    }
}

bool is_full() {
    if (FRONT == 0 && REAR + 1 == CAPACITY) {
        return true;
    } else {
        return false;
    }
}

void display() {
    int i;

    for (i = 0; i <= REAR; i++) {
        printf("[%d]. %d\n", i + 1, QUEUE[i]);
    }
}

void enqueue(int item) {
    if (is_full()) {
        printf("Cannot enqueue, queue is full.");
        exit(1);
    }

    printf("Enqueueing item: %d\n", item);

    if (FRONT == -1)
        FRONT++;

    QUEUE[++REAR] = item;
}

int dequeue() {
    if (is_empty()) {
        printf("Cannot dequeue, queue is empty.");
        exit(1);
    }

    int item = QUEUE[FRONT];
    int total = CAPACITY - 1;
    int i;

    for (i = 0; i < total; i++) {
        QUEUE[i] = QUEUE[i + 1];
    }

    REAR--;

    return item;
}

int main() {
    enqueue(1);
    enqueue(2);
    enqueue(3);
    enqueue(4);
    enqueue(5);

    printf("\nQueue before dequeueing:\n");
    display();

    dequeue();

    printf("\nQueue after dequeueing:\n");
    display();

    return 0;
}