// @ts-check

class Queue {
    /**
     * @param {number} capacity 
     */
    constructor(capacity) {
        this.capacity = capacity;
        /**
         * @type {Array<number>}
         */
        this.data = new Array(this.capacity);
        this.front = -1;
        this.rear = -1;
    }

    get size() {
        return this.capacity - 1;
    }

    isFull() {
        return this.front === 0 && this.rear === this.size;
    }

    isEmpty() {
        return this.front === -1;
    }

    /**
     * @param {number} item 
     */
    enqueue(item) {
        if (this.isFull()) {
            console.log('Cannot enqueue, queue is full');
            process.exit(1);
        }

        console.log(`Enqueueing item ${item}`);

        if (this.front === -1) this.front++;

        this.data[++this.rear] = item;
    }

    dequeue() {
        if (this.isEmpty()) {
            console.log('Cannot dequeue, queue is empty');
            process.exit(1);
        }

        const item = this.data[this.front];

        for (let i = 0; i < this.size; i++) {
            this.data[i] = this.data[i + 1];
        }

        this.rear--;

        return item;
    }

    display() {
        for (let i = 0; i <= this.rear; i++) {
            console.log('[%d]. %d', i + 1, this.data[i]);
        }
    }

    static main() {
        const queue = new Queue(5);

        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        queue.enqueue(4);
        queue.enqueue(5);

        console.log("\nQueue before dequeueing:");
        queue.display();

        queue.dequeue();

        console.log("\nQueue after dequeueing:");
        queue.display();
    }
}

// execute
Queue.main();