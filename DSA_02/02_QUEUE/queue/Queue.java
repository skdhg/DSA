public class Queue {
    private int[] data;
    private int capacity;
    private int front;
    private int rear;

    public Queue(int capacity) {
        this.capacity = capacity;
        this.data = new int[this.capacity];
        this.front = -1;
        this.rear = -1;
    }

    public int getSize() {
        return this.capacity - 1;
    }

    public boolean isFull() {
        return this.front == 0 && this.rear == this.getSize();
    }

    public boolean isEmpty() {
        return this.front == -1;
    }

    public void display() {
        for (int i = 0; i <= this.rear; i++) {
            System.out.printf("[%d]. %d\n", i + 1, this.data[i]);
        }
    }

    public void enqueue(int item) {
        if (this.isFull()) {
            System.out.println("Cannot enqueue, stack is full");
            System.exit(1);
        }

        if (this.front == -1) {
            this.front++;
        }

        this.data[++this.rear] = item;
    }

    public int dequeue() {
        if (this.isEmpty()) {
            System.out.println("Cannot dequeue, stack is empty");
            System.exit(1);
        }

        int total = this.getSize();
        int item = this.data[this.front];

        for (int i = 0; i < total; i++) {
            this.data[i] = this.data[i + 1];
        }

        this.rear--;

        return item;
    }

    public static void main(String[] args) {
        Queue queue = new Queue(5);

        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        queue.enqueue(4);
        queue.enqueue(5);

        System.out.println("\nQueue before dequeueing:");
        queue.display();

        queue.dequeue();

        System.out.println("\nQueue after dequeueing:");
        queue.display();
    }
}