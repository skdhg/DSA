// @ts-check

class Stack {
    /**
     * @param {number} capacity 
     */
    constructor(capacity) {
        this.capacity = capacity;
        /**
         * @type {Array<number>}
         */
        this.data = new Array(this.capacity);
        this.top = -1;
    }

    size() {
        return this.top + 1;
    }

    isEmpty() {
        return this.top === -1;
    }

    isFull() {
        return this.top + 1 === this.capacity;
    }

    /**
     * @param {number} item 
     */
    push(item) {
        if (this.isFull()) {
            console.log('Cannot push, stack overflow.');
            process.exit(1);
        }

        console.log(`Pushing item: ${item}`);
        this.data[++this.top] = item;
    }

    pop() {
        if (this.isEmpty()) {
            console.log('Cannot pop, stack is empty.');
            process.exit(1);
        }

        return this.data[this.top--];
    }

    printStack() {
        for (let i = 0; i <= this.top; i++) {
            console.log('[%d]. %d', i + 1, this.data[i]);
        }
    }

    static main() {
        const stack = new Stack(5);

        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);

        console.log("\nBefore popping out");
        stack.printStack();
        console.log();

        stack.pop();

        console.log("After popping out");
        stack.printStack();
        console.log();
    }
}

// execute
Stack.main();