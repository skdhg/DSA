#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define CAPACITY 5

int TOP = -1;
int STACK[CAPACITY];

bool is_empty() {
    if (TOP == -1) {
        return true;
    } else {
        return false;
    }
}

bool is_full() {
    if (TOP + 1 == CAPACITY) {
        return true;
    } else {
        return false;
    }
}

void push(int item) {
    if (is_full()) {
        printf("Cannot push, stack overflow.");
        exit(1);
    }

    printf("Pushing item: %d\n", item);

    STACK[++TOP] = item;
}

int pop() {
    if (is_empty()) {
        printf("Cannot pop, stack is empty.");
        exit(1);
    }

    return STACK[TOP--];
}

int peek() {
    if (is_empty()) {
        printf("Cannot peek, stack is empty.");
        exit(1);
    }

    return STACK[TOP];
}

int size() { return TOP + 1; }

void print_stack() {
    int i;

    for (i = 0; i <= TOP; i++) {
        printf("[%d]. %d\n", i + 1, STACK[i]);
    }
}

int main() {
    push(1);
    push(2);
    push(3);
    push(4);

    printf("\nBefore popping out\n");
    print_stack();
    printf("\n");

    pop();

    printf("\nAfter popping out\n");
    print_stack();
    printf("\n");

    return 0;
}