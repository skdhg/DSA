public class Stack {
    private int top = -1;
    private int capacity;
    private int[] data;

    public Stack(int capacity) {
        this.capacity = capacity;
        this.data = new int[this.capacity];
        this.top = -1;
    }

    public int size() {
        return this.top + 1;
    }

    public boolean isFull() {
        return this.top + 1 == this.capacity;
    }

    public boolean isEmpty() {
        return this.top == -1;
    }

    public void printStack() {
        for (int i = 0; i <= this.top; i++) {
            System.out.printf("[%d]. %d\n", i + 1, this.data[i]);
        }
    }

    public void push(int item) {
        if (this.isFull()) {
            System.out.println("Error: Stack overflow");
            System.exit(1);
        }

        System.out.println("Inserting item: " + item);
        this.data[++this.top] = item;
    }

    public int pop() {
        if (this.isEmpty()) {
            System.out.println("Error: Cannot pull, stack is empty");
            System.exit(1);
        }

        return this.data[this.top--];
    }

    public int peek() {
        if (this.isEmpty()) {
            System.out.println("Error: Cannot peek, stack is empty");
            System.exit(1);
        }

        return this.data[this.top];
    }

    public static void main(String[] args) {
        Stack stack = new Stack(5);

        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);

        System.out.println("\nBefore popping out");
        stack.printStack();
        System.out.println();

        stack.pop();

        System.out.println("After popping out");
        stack.printStack();
        System.out.println();
    }
}